#!/bin/sh
# macOS
export DYLD_LIBRARY_PATH=`pwd`/ft_malloc
export DYLD_INSERT_LIBRARIES=`pwd`/ft_malloc/libft_malloc.so
export DYLD_FORCE_FLAT_NAMESPACE=1
# linux
export LD_LIBRARY_PATH=`pwd`/ft_malloc
export LD_PRELOAD=`pwd`/ft_malloc/libft_malloc.so
$@
