.PHONY: all test compile

all: test0 test1 test2 test3_1 test3_2 test4 test5 ft_malloc/libft_malloc.so

OS=$(shell uname)
COMPILE = gcc -Wall -Werror -Wextra
ifeq ($(OS),Linux)
	COMPILE += -Wno-unused-but-set-variable
else
	COMPILE += -Wno-unused-result
endif
ifeq ($(DEBUG),1)
	COMPILE += -g
endif


ft_malloc/libft_malloc.so:
	cd ft_malloc && $(MAKE)  

test0: srcs/test0.c
	$(COMPILE) -o test0 srcs/test0.c

test1: srcs/test1.c
	$(COMPILE) -o test1 srcs/test1.c

test2: srcs/test2.c
	$(COMPILE) -o test2 srcs/test2.c

test3_1: srcs/test3_1.c
	$(COMPILE) -o test3_1 srcs/test3_1.c

test3_2: srcs/test3_2.c
	$(COMPILE) -o test3_2 srcs/test3_2.c

test4: srcs/test4.c
	$(COMPILE) -o test4 srcs/test4.c

test5: srcs/test5.c ft_malloc/libft_malloc.so
	$(COMPILE) -o test5 srcs/test5.c -L./ft_malloc -lft_malloc

clean:
	rm test*

re:
	$(MAKE) clean
	$(MAKE) all
