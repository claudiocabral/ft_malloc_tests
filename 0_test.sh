#!/bin/sh

if [[ $(uname) = Linux ]]
then
	function count_pages()
	{
		local result=$1
		TMP=`/usr/bin/time -f %R ${@:2} 2>&1`
		if [[ $? != 0 ]]
		then
			echo Test 0 failed, error message:
			echo $TMP
			exit 1
		fi
		eval $result="'$TMP'"
	}
else
	function count_pages()
	{
		local result=$1
		TMP=`/usr/bin/time -l ${@:2} 2>&1`
		if [[ $? != 0 ]]
		then
			echo Test 0 failed, error message:
			echo $TMP
			exit 1
		fi
		TMP=`grep <<<"$TMP" 'page reclaims' | awk '{print $1}'`
		eval $result="'$TMP'"
	}
fi

count_pages COUNT_SYSTEM ./test0
echo test0: Number of pages in malloc $COUNT_SYSTEM
count_pages COUNT_FT ./run.sh ./test0
echo test0: Number of pages  in ft_malloc $COUNT_FT
python -c "print($COUNT_FT - $COUNT_SYSTEM)"
count_pages COUNT_FT_1 ./run.sh ./test1
echo test1: Number of pages  in ft_malloc $COUNT_FT_1
python -c "print($COUNT_FT_1 * 4096)"
